package manejointerfaces;

import datos.*;

public class ManejoInterfaces {

    public static void main(String[] args) {
        AccesoDatos datos = new ImplementacionOracle();
        ejecutar(datos, "listar");
        
        datos = new ImplementacionMySql();
        ejecutar(datos,"Insertar");
    }
    
    private static void ejecutar(AccesoDatos datos, String accion){
        if("listar".equals(accion)){
            datos.listar();
        }
        else if("Insertar".equals(accion)){
            datos.insertar();
        }
    }
}

//esto es todo
